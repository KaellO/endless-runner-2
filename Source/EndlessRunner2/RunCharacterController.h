// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacter.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNER2_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

	public:
	// Sets default values for this controller's properties
	ARunCharacterController();

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	ARunCharacter* RunCharacter;
	void MoveRight(float scale);

	public:	
	virtual void SetupInputComponent() override;
};
