// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("ArrowComponent");
	ArrowComponent->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(SceneComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBoxBeginOverlap);

	ObstacleArea = CreateDefaultSubobject<UBoxComponent>("ObstacleArea");
	ObstacleArea->SetupAttachment(SceneComponent);

	PickupArea = CreateDefaultSubobject<UBoxComponent>("PickupArea");
	PickupArea->SetupAttachment(SceneComponent);

	ObstacleSpawnRate = 60;
	PickupSpawnRate = 30;
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	int32 SpawnOb = FMath::RandRange(0, 100); 
	if(SpawnOb <= ObstacleSpawnRate)
	{
		SpawnObstacle();
	}

	int32 SpawnPick = FMath::RandRange(0, 100); 
	if(SpawnPick <= PickupSpawnRate)
	{
		SpawnPickup();
	}
}

void ATile::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
							UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, 
							const FHitResult& SweepResult)
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));
	if(ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{	
		OnExited.Broadcast(this);
	} 
}

void ATile::SpawnObstacle()
{
	TSubclassOf<class AObstacle> ObstacleClassChosen;

	int32 randInex = FMath::RandRange(0, ObstacleClass.Num() - 1);
   	ObstacleClassChosen = ObstacleClass[randInex];

	FVector SpawnArea = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleArea->GetRelativeLocation(), ObstacleArea->GetScaledBoxExtent());

	if(ObstacleClassChosen)
	{
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;

		AObstacle* obstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClassChosen, SpawnArea, Rotation, SpawnInfo);

		obstacle->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void ATile::SpawnPickup()
{
	TSubclassOf<class APickup> PickupClassChosen;

	int32 randInex = FMath::RandRange(0, PickupClass.Num() - 1);
   	PickupClassChosen = PickupClass[randInex];

	FVector SpawnArea = UKismetMathLibrary::RandomPointInBoundingBox(PickupArea->GetRelativeLocation(), PickupArea->GetScaledBoxExtent());

	if(PickupClassChosen)
	{
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;

		APickup* pickup = GetWorld()->SpawnActor<APickup>(PickupClassChosen, SpawnArea, Rotation, SpawnInfo);

		pickup->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

FVector ATile::GetArrowLocation()
{
	return ArrowComponent->GetComponentLocation();
}

void ATile::DestroyActorWithDelay(float time)
{
	GetWorld()->GetTimerManager().SetTimer(DestroyTimerHandle, this, &ATile::DestroyActor, time, false);
}

void ATile::DestroyActor()
{
	TArray<AActor*> attachedActors;
	GetAttachedActors(attachedActors);

	for (int i = 0; i < attachedActors.Num(); i++) {
   		attachedActors[i]->Destroy();
 	}

	if (this->IsValidLowLevel())
	{
		this->Destroy();
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

