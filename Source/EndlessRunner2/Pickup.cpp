// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(SceneComponent);

	OnActorBeginOverlap.AddDynamic(this, &APickup::OnBeginOverlap);

}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickup::OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if(ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{	
		OnGet(character);
		this->Destroy();
	} 
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

