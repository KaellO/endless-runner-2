// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tile.h"
#include "RunCharacter.h"
#include "EndlessRunner2GameModeBase.generated.h"

UCLASS()
class ENDLESSRUNNER2_API AEndlessRunner2GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	public:
	UFUNCTION()
	void ExitedTile(ATile* tileExited);

	protected:
	UPROPERTY(EDITANYWHERE)
	TSubclassOf<class ATile> TileClass;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	private:
	ATile* SpawnTile();
	FVector Location;
};
