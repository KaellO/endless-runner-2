// Copyright Epic Games, Inc. All Rights Reserved.


#include "EndlessRunner2GameModeBase.h"

void AEndlessRunner2GameModeBase::BeginPlay()
{
	Super::BeginPlay();
    Location = FVector(0, 0, 0);

    for(int i = 0; i <= 5; i++)
    {
        SpawnTile();
    }
}

ATile* AEndlessRunner2GameModeBase::SpawnTile()
{
    if(TileClass)
    {
        FRotator Rotation(0.0f, 0.0f, 0.0f);
        FActorSpawnParameters SpawnInfo;

        ATile* tile = GetWorld()->SpawnActor<ATile>(TileClass, Location, Rotation, SpawnInfo);
        Location = tile->GetArrowLocation();

        tile->OnExited.AddDynamic(this, &AEndlessRunner2GameModeBase::ExitedTile);
        
        return tile;
    }
    else
        return NULL;
}

void AEndlessRunner2GameModeBase::ExitedTile(ATile* tileExited)
{
    ATile* tile = SpawnTile();

    if (tileExited)
    {
        tileExited->DestroyActorWithDelay(0.5f);
    }
}
