// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Tile.generated.h"
 
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExited, ATile*, Tile);

UCLASS()
class ENDLESSRUNNER2_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	FVector GetArrowLocation();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UArrowComponent* ArrowComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ObstacleArea;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* PickupArea;

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
							UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, 
							const FHitResult& SweepResult);

	void SpawnObstacle();
	void SpawnPickup();

	UPROPERTY(EDITANYWHERE)
	TArray<TSubclassOf<class AObstacle>> ObstacleClass;

	UPROPERTY(EDITANYWHERE)
	TArray<TSubclassOf<class APickup>> PickupClass;

	int32 ObstacleSpawnRate;
	int32 PickupSpawnRate;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintAssignable, Category = "Exited")
	FExited OnExited;
	FTimerHandle DestroyTimerHandle;
	void DestroyActor();
	void DestroyActorWithDelay(float time);
};
