// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "RunCharacterController.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleCollider = CreateDefaultSubobject<UCapsuleComponent>("CapsuleCollider");
	SetRootComponent(CapsuleCollider);

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>("SkeletalMesh");
	SkeletalMesh->SetupAttachment(CapsuleCollider);

	Movement = CreateDefaultSubobject<UFloatingPawnMovement>("Movement");

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->SetupAttachment(CapsuleCollider);
	SpringArm->TargetArmLength = 300.0f;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);

	IsDead = false;
	coins = 0;
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(!IsDead)
	{
		MoveForward(1.0);
	}
}

void ARunCharacter::MoveForward(float scale)
{
	Movement->AddInputVector(GetActorForwardVector() * scale);
}

void ARunCharacter::MoveRight(float scale)
{
	if(!IsDead)
	{
		Movement->AddInputVector(GetActorRightVector() * scale);
	}
	
}

void ARunCharacter::Die()
{
	if(!IsDead)
	{
		APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

		UGameplayStatics::GetPlayerPawn(this, 0)->DisableInput(PlayerController);

		SkeletalMesh->ToggleVisibility();

		OnPlayerDeath.Broadcast();

		IsDead = true;

		OnDeath();
	}
}

void ARunCharacter::AddCoin()
{
	coins++;
}

