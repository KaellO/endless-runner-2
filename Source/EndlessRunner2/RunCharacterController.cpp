// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacterController.h"


ARunCharacterController::ARunCharacterController()
{

}

// Called when the game starts or when spawned
void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
    RunCharacter = Cast<ARunCharacter>(AController::GetPawn());
}

void ARunCharacterController::MoveRight(float scale)
{
    RunCharacter->MoveRight(scale);
}

void ARunCharacterController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

